package com.humennyi;

import com.humennyi.controller.CharacterManager;
import com.humennyi.model.Elf;
import com.humennyi.model.Fraction;
import com.humennyi.model.Human;
import com.humennyi.model.Orc;
import com.humennyi.model.Character;

import java.util.LinkedList;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        List<Character> character = new LinkedList<>();

        character.add(new Human("Human1", 70, 25, 20, 30, Fraction.ALLIANCE));
        character.add(new Orc("Orc1", 100, 50, 5, 50, Fraction.HORDE));
        character.add(new Elf("Elf1", 85, 35, 10, 30, Fraction.ALLIANCE));
        character.add(new Human("Human2", 75, 30, 20, 35, Fraction.ALLIANCE));
        character.add(new Orc("Orc2", 105, 55, 2, 55, Fraction.HORDE));
        character.add(new Elf("Elf2", 80, 40, 15, 25, Fraction.HORDE));
        character.add(new Human("Human2", 95, 20, 25, 30, Fraction.ALLIANCE));
        character.add(new Orc("Orc", 85, 60, 3, 60, Fraction.HORDE));

        CharacterManager characterManager = new CharacterManager(character);
        System.out.println("All characters:");
        characterManager.print();
        System.out.println(" \033[31;1m =====HORDE=====" + "\033[0m ");
        System.out.println(characterManager.getHorde());
        System.out.println(" \033[34;1m =====ALLIANCE=====" + "\033[0m ");
        System.out.println(characterManager.getAlliance());
        System.out.println("=====FILTER=====");
        System.out.println(characterManager.filter(55, 66));

    }
}
