package com.humennyi.constants;

public final class Constants {
    public static final double ELF_COEF_STRENGTH = 1.2;
    public static final double ELF_COEF_INTEL = 0.25;
    public static final double ELF_COEF_HEALTH = 1.55;
    public static final int ELF_COEF_ARMOR = 50;

    public static final double HUMAN_COEF_STRENGTH = 1.1;
    public static final double HUMAN_COEF_INTEL = 0.5;
    public static final double HUMAN_COEF_HEALTH = 1.5;
    public static final int HUMAN_COEF_ARMOR = 75;

    public static final double ORC_COEF_STRENGTH = 1.3;
    public static final double ORC_COEF_INTEL = 0.1;
    public static final double ORC_COEF_HEALTH = 1.8;
    public static final int ORC_COEF_ARMOR = 90;

    private Constants() {}
}
