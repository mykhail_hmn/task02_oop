package com.humennyi.controller;

import com.humennyi.model.Fraction;
import com.humennyi.model.Character;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class CharacterManager {
    private final List<Character> charactersList;


    public CharacterManager(List<Character> charactersList) {
        this.charactersList = charactersList;
    }

    public void print() {
        Stream<Character> streamFromCollection = charactersList.stream();
        streamFromCollection.forEach(System.out::println);

    }

    public List<Character> getHorde() {
      List<Character> horde = charactersList.stream()
        .filter((p) -> p.getFraction() == Fraction.HORDE)
                            .collect(Collectors.toList());

        return horde;
    }

    public List<Character> getAlliance() {
        List<Character> alliance = charactersList.stream()
                .filter((p) -> p.getFraction() == Fraction.ALLIANCE)
                .collect(Collectors.toList());

        return alliance;
    }

    /**
     * Show character with attack which entry in range
     * @param attack1
     * @param attack2
     * @return ArrayList with filtered characters
     */
    public List<Character> filter(int attack1,int attack2) {
        List<Character> filter = charactersList.stream()
                .filter((p)-> p.getAttack() >= attack1 && p.getAttack() < attack2)
                .collect(Collectors.toList());

        return filter;
    }
}


