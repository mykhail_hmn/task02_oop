package com.humennyi.model;

import java.io.Serializable;

public abstract class Character implements Serializable {
    static int idCounter;
    int id = 0;
    String name;
    private double hp;
    private int strength;
    private int intelligence;
    private int protection;
    private Fraction fraction;
    double attack;


    public Character() {
        idCounter++;
        id = idCounter;
    }

    public Character(String name, double hp, int strength, int intelligence, int protection, Fraction fraction) {
        this.name = name;
        this.hp = hp;
        this.strength = strength;
        this.intelligence = intelligence;
        this.protection = protection;
        this.fraction = fraction;

    }

    abstract double getAttackStrength();

    abstract double getHealth();

    abstract int getArmor();

    public int getId() {

        return id;
    }

    public String getName() {

        return name;
    }

    public Fraction getFraction() {

        return fraction;
    }

    public double getHp() {
        return hp;
    }

    public double getAttack() {
        return attack;
    }

    public int getProtection() {
        return protection;
    }

    public int getStrength() {
        return strength;
    }

    public int getIntelligence() {
        return intelligence;
    }

    @Override
    public String toString() {
        if (fraction == Fraction.HORDE) {
            return "Race: " + getClass() + " Name: " + name + ": " + " Attack: "
                    + getAttackStrength() + " Armor: " + getArmor() + " \033[31;1mFraction:" + getFraction() + "\033[0m ";
        } else if (fraction == Fraction.ALLIANCE) {
            return "Race: " + getClass() + " Name: " + name + ": " + " Attack: "
                    + getAttackStrength() + " Armor: " + getArmor() + " \033[34;1mFraction:" + getFraction() + "\033[0m ";
        } else return "";
    }
}
