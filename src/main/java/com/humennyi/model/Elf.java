package com.humennyi.model;

import static com.humennyi.constants.Constants.*;

public class Elf extends Character {


    public Elf(String name, double hp, int strength,
               int intelligence, int protection, Fraction fraction) {
        super(name, hp, strength, intelligence, protection, fraction);
    }

    @Override
    public double getAttackStrength() {
        return this.attack = getStrength() * ELF_COEF_STRENGTH + getIntelligence() * ELF_COEF_INTEL;
    }

    @Override
    double getHealth() {
        return getHp() * ELF_COEF_HEALTH;
    }

    @Override
    int getArmor() {
        return getProtection() * ELF_COEF_ARMOR;
    }


}
