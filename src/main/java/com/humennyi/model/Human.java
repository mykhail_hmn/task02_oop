package com.humennyi.model;

import static com.humennyi.constants.Constants.*;

public class Human extends Character {

    public Human(String name, double hp, int strength,
                 int intelligence, int protection, Fraction fraction) {
        super(name, hp, strength, intelligence, protection, fraction);

    }

    @Override
    public double getAttackStrength() {
        return this.attack = getStrength() * HUMAN_COEF_STRENGTH + getIntelligence() * HUMAN_COEF_INTEL;
    }

    @Override
    double getHealth() {
        return getHp() * HUMAN_COEF_HEALTH;
    }

    @Override
    int getArmor() {
        return getProtection() * HUMAN_COEF_ARMOR;
    }
}
