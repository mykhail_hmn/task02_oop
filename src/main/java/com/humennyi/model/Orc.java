package com.humennyi.model;

import static com.humennyi.constants.Constants.*;

public class Orc extends Character {


    public Orc(String name, double hp, int strength,
               int intelligence, int protection, Fraction fraction) {
        super(name, hp, strength, intelligence, protection, fraction);

    }

    @Override
    public double getAttackStrength() {

        return this.attack = getStrength() * ORC_COEF_STRENGTH + getIntelligence() * ORC_COEF_INTEL;
    }

    @Override
    double getHealth() {
        return getHp() * ORC_COEF_HEALTH;
    }

    @Override
    int getArmor() {
        return getProtection() * ORC_COEF_ARMOR;
    }
}
